'use strict'

//global variables
let taskCounterTag = document.getElementsByTagName('output')[0];
let taskQuantity = document.getElementsByClassName('list-block')[0].getElementsByTagName('input').length;
let tasks = Array.from(document.getElementsByClassName('list-block')[0].getElementsByTagName('input'));

//functions
function taskChange() {
  let taskCounter = tasks.filter(task => {
    return task.checked === true;
  });
  taskCounterTag.value = `${taskCounter.length} из ${taskQuantity}`;
  if (taskCounter.length === taskQuantity){
    document.getElementsByClassName('list-block')[0].classList.add('complete');
  } else {
    document.getElementsByClassName('list-block')[0].classList.remove('complete');
  }
}

//code
document.addEventListener('DOMContentLoaded', taskChange);

tasks.forEach(task =>{
  task.addEventListener('input', taskChange);
})