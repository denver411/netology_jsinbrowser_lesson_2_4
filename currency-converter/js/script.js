'use strict'

//global variables
let currencies;
const inputSum = document.getElementById('source');
const currencyFrom = document.getElementById('from');
const currencyTo = document.getElementById('to');

//functions
function onLoadStart() {
  document.getElementById('loader').classList.remove('hidden');
}

function onLoadEnd() {
  if (currencyRequest.status === 200) {
    currencies = JSON.parse(currencyRequest.responseText);
    currencies.forEach(element => {
      currencyFrom.innerHTML += `<option value='${element.code}'>${element.code}</option>`;
      currencyTo.innerHTML += `<option value='${element.code}'>${element.code}</option>`;
    });
    document.getElementById('loader').classList.add('hidden');
    document.getElementById('content').classList.remove('hidden');
  }
}

function convertCurrency() {
  let currencyValueFrom = currencies.find(cur => {
    return cur.code === currencyFrom.value;
  });
  let currencyValueTo = currencies.find(cur => {
    return cur.code === currencyTo.value;
  });
  document.getElementById('result').value = (inputSum.value * currencyValueFrom.value / currencyValueTo.value).toFixed(2);
}

//code
const currencyRequest = new XMLHttpRequest();
currencyRequest.open('GET', 'https://neto-api.herokuapp.com/currency', true);
currencyRequest.addEventListener('loadstart', onLoadStart);
currencyRequest.addEventListener('load', onLoadEnd);
currencyRequest.send();

inputSum.addEventListener('input', convertCurrency);
currencyFrom.addEventListener('input', convertCurrency);
currencyTo.addEventListener('input', convertCurrency);