'use strict'

//global variables
const inputs = Array.from(document.getElementsByClassName('contentform')[0].getElementsByTagName('input'));
inputs.push(document.getElementsByTagName('textarea')[0]);
const outputs = Array.from(document.getElementById('output').getElementsByTagName('output'));
const submitButton = document.getElementsByClassName('contentform')[0].getElementsByClassName('button-contact')[0];
const editButton = document.getElementById('output').getElementsByClassName('button-contact')[0];

//functions
function zipValipate() {
  const zipCheck = new RegExp(/^\d+$/);
  if (!zipCheck.test(zipInput.value)) {
    zipInput.value = zipInput.value.substring(0, zipInput.value.length - 1);
  }
}

function checkAllInputsFill() {
  const inputsNotEmpty = inputs.filter(item => {
    return item.value.trim() !== '';
  });
  if (inputsNotEmpty.length === inputs.length) {
    submitButton.removeAttribute('disabled');
  } else {
    submitButton.setAttribute('disabled', null);
  }
}

function submitForm(event) {
  event.preventDefault();
  outputs.forEach(output => {
    output.innerHTML = inputs.find(item => {
      return item.name === output.id;
    }).value;
  });
  document.getElementsByClassName('contentform')[0].classList.add('hidden');
  document.getElementById('output').classList.remove('hidden');
}

function editForm() {
  document.getElementsByClassName('contentform')[0].classList.remove('hidden');
  document.getElementById('output').classList.add('hidden');
}

//code
// zip input validate event
const zipInput = inputs.find(item => {
  return item.name === 'zip';
});
zipInput.addEventListener('input', zipValipate);

// on change inputs&buttons events
inputs.forEach(item => {
  item.addEventListener('change', checkAllInputsFill);
});

submitButton.addEventListener('click', submitForm);

editButton.addEventListener('click', editForm);